import { initialize, mswDecorator } from 'msw-storybook-addon';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'assets/global.module.css';

import { store } from '../src/utils/store';

initialize();

export const parameters = {
	actions: { argTypesRegex: '^on[A-Z].*' },
	controls: {
		matchers: {
			color: /(background|color)$/i,
			date: /Date$/,
		},
	},
	layout: 'fullscreen',
};

export const decorators = [
	mswDecorator,
	Story => {
		return (
			<Provider store={store}>
				<Story />
			</Provider>
		);
	},
];
