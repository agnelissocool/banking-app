import { Suspense, lazy } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import { store } from 'utils/store';

const LoginPage = lazy(() => import('pages/login/Login'));
const ProductsPage = lazy(() => import('pages/products/Products'));
const NotFoundPage = lazy(() => import('pages/not-found/NotFound'));

const App = () => {
	return (
		<Provider store={store}>
			<Suspense fallback={<></>}>
				<Container fluid>
					<Row>
						<BrowserRouter>
							<Routes>
								<Route path="/" element={<LoginPage />} />
								<Route path="/products" element={<ProductsPage />} />
								<Route path="/404" element={<NotFoundPage />} />
								<Route path="*" element={<Navigate replace to="/404" />} />
							</Routes>
						</BrowserRouter>
					</Row>
				</Container>
			</Suspense>
		</Provider>
	);
};

export default App;
