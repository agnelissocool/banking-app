import { rest } from 'msw';

export const beneficiariesMock = () => {
	return rest.get(`http://localhost:500/api/v1/beneficiaries`, (req, res, ctx) => {
		return res(ctx.json([]));
	});
};
