import { rest } from 'msw';

export const loginMock = () => {
	return rest.post(`https://jsonplaceholder.typicode.com/api/v1/login`, (req, res, ctx) => {
		return res(
			ctx.json({
				user: {
					id: 19879798,
					name: 'John Doe',
					hash: '8koa33WcREigvgMjoIxtYYnKhUE3fNcm',
				},
			})
		);
	});
};
