import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';

import logo from 'assets/logo.svg';
import styles from './header.module.css';

const Header = () => {
	return (
		<Row>
			<Navbar bg="dark" variant="dark">
				<Container>
					<Navbar.Brand href="#home">
						<img alt="banking" src={logo} width="30" height="30" className="d-inline-block align-top" /> Online banking
					</Navbar.Brand>
					<Navbar.Collapse className={styles['collapse']}>
						<p className={styles['user']}>Welcome Agnel</p>
						<Button className={styles['logout']} variant="light">
							Logout
						</Button>
					</Navbar.Collapse>
				</Container>
			</Navbar>
		</Row>
	);
};

export default Header;
