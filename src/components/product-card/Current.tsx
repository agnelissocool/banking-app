import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

import styles from './productcard.module.css';

const Current = () => {
	return (
		<Card className={styles['product-card']}>
			<Card.Body className={styles['product-body']}>
				<Card.Title>Credit Card</Card.Title>
				<Card.Subtitle className="mb-2 text-muted">You do not have a credit card</Card.Subtitle>
				<Card.Text>Please use the link below to apply for a new one</Card.Text>

				<Button variant="primary" className={styles['product-button']}>
					<Card.Link target="_blank" href="http://emiratesnbd.com" className={styles['product-link']}>
						Apply
					</Card.Link>
				</Button>
			</Card.Body>
		</Card>
	);
};

export default Current;
