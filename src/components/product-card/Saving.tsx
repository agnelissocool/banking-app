import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Stack from 'react-bootstrap/Stack';

import styles from './productcard.module.css';

const Savings = () => {
	return (
		<Card className={styles['product-card']}>
			<Card.Body className={styles['product-body']}>
				<Card.Title>Saving Account</Card.Title>
				<Card.Subtitle className="mb-2 text-muted">Marhaba Saving Account</Card.Subtitle>
				<Card.Text>CIF: 6882</Card.Text>
				<Card.Text>Account Number: 6882 38991</Card.Text>
				<Card.Text>Balance: AED 100,000,000</Card.Text>
				<Stack direction="horizontal" gap={3}>
					<Button variant="primary" className={styles['product-button']}>
						<Card.Link target="_blank" href="http://emiratesnbd.com" className={styles['product-link']}>
							Details
						</Card.Link>
					</Button>
					<Button variant="primary" className={styles['product-button']}>
						<Card.Link target="_blank" href="http://emiratesnbd.com" className={styles['product-link']}>
							Transfer
						</Card.Link>
					</Button>
				</Stack>
			</Card.Body>
		</Card>
	);
};

export default Savings;

<Card style={{ width: '18rem' }}>
	<Card.Body>
		<Card.Link href="#">Another Link</Card.Link>
	</Card.Body>
</Card>;
