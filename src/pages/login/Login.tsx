import { useCallback, useEffect, useRef, useState } from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import { Row, Form, Col } from 'react-bootstrap';
import Alert from 'react-bootstrap/Alert';
import { useAppSelector, useAppDispatch } from 'hooks/useRedux';
import { useNavigate } from 'react-router-dom';
import Spinner from 'react-bootstrap/Spinner';

import { fetchLoginThunk } from 'slices/userSlice';
import styles from './login.module.css';
import logo from 'assets/logo.svg';

const Login = () => {
	const [loginError, setLoginError] = useState<boolean>(false);
	const username = useRef<HTMLInputElement>(null);
	const password = useRef<HTMLInputElement>(null);
	const user = useAppSelector(state => state.user);
	const dispatch = useAppDispatch();
	const navigate = useNavigate();

	const handleLogin = useCallback(() => {
		const user = username.current?.value;
		const pass = password.current?.value;

		if (!user || !pass) {
			return setLoginError(true);
		}

		dispatch(fetchLoginThunk({ username: user, password: pass }));
	}, [dispatch]);

	useEffect(() => {
		if (user.data.name !== '') {
			navigate('/products');
		}
	}, [user, navigate]);

	return (
		<Container className={styles['login']}>
			<Row>
				<Col md={6}>
					<Form className={styles['form']}>
						<h2>Login</h2>

						<Form.Group style={{ width: '400px' }} className="mb-3 pt-3" controlId="formBasicEmail">
							<Form.Label>User ID</Form.Label>
							<Form.Control ref={username} type="text" placeholder="Enter username" />
							<Form.Text className="text-muted"></Form.Text>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicPassword">
							<Form.Label>Password</Form.Label>
							<Form.Control ref={password} type="password" placeholder="Enter password" />
						</Form.Group>

						{!user.isLoading && (
							<Button variant="primary" onClick={handleLogin}>
								Continue
							</Button>
						)}

						{user.isLoading && (
							<Spinner animation="border" variant="primary" role="status">
								<span className="visually-hidden">Loading...</span>
							</Spinner>
						)}
					</Form>

					{loginError && (
						<Alert className="mt-3" style={{ padding: '10px' }} variant="danger">
							Invalid username or password.
						</Alert>
					)}
				</Col>
				<Col md={6}>
					<div className="text-center">
						<img alt="banking" src={logo} width="200" height="200" className="d-inline-block align-top" />
						<h2>Digital Business Capability Fund Transfer </h2>
					</div>
				</Col>
			</Row>
		</Container>
	);
};
export default Login;
