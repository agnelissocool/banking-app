import styles from './notfound.module.css';

const NotFound = () => {
	return (
		<div className={styles['not-found']}>
			<h1 className={styles['title']}>404</h1>
			<p>Page not found.</p>
		</div>
	);
};

export default NotFound;
