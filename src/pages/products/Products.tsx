import Header from 'components/header/Header';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Saving from 'components/product-card/Saving';
import Current from 'components/product-card/Current';
import CreditCard from 'components/product-card/CreditCard';
import styles from './products.module.css';

const Products = () => {
	return (
		<div className={styles['products']}>
			<Header />
			<Container>
				<Row>
					<h2 className={styles['title']}>Your Banking Products</h2>
				</Row>

				<Row>
					<Saving />
					<Current />
					<CreditCard />
				</Row>
			</Container>
		</div>
	);
};

export default Products;
