import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { User } from 'types/index';

export const beneficiarySlice = createApi({
	reducerPath: 'posts',
	keepUnusedDataFor: 1000,
	baseQuery: fetchBaseQuery({
		baseUrl: 'http://localhost:500/api/v1/beneficiaries',
		prepareHeaders(headers) {
			headers.set('content-type', 'application/json');
			return headers;
		},
	}),
	tagTypes: ['Posts'],
	endpoints: builder => ({
		fetchPosts: builder.query<User[], void>({
			query: () => `/posts`,
			providesTags: [{ type: 'Posts', id: 'LIST' }],
		}),
	}),
});

export const { useFetchPostsQuery } = beneficiarySlice;
