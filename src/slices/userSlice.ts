import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { User } from 'types/index';

interface UserState {
	data: User;
	isLoading: boolean;
	error: boolean;
}

const initialState: UserState = {
	data: {
		id: 0,
		name: '',
		username: '',
		hash: '',
	},
	isLoading: false,
	error: false,
};

export const fetchLoginThunk = createAsyncThunk('user/fetchLogin', async ({ username, password }: { username: string; password: string }) => {
	await new Promise(resolve => setTimeout(resolve, 1000));
	const cities = await fetch(`https://jsonplaceholder.typicode.com/api/v1/login`, {
		method: 'POST',
		body: JSON.stringify({
			username,
			password,
		}),
	});
	const response = await cities.json();
	return response;
});

export const counterSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {
		add: state => {},
		clear: state => {
			state = initialState;
		},
	},
	extraReducers(builder) {
		builder
			.addCase(fetchLoginThunk.pending, state => {
				state.isLoading = true;
			})
			.addCase(fetchLoginThunk.fulfilled, (state, action) => {
				state.isLoading = false;
				state.data = action.payload;
			})
			.addCase(fetchLoginThunk.rejected, (state, action) => {
				state.isLoading = false;
				state.error = true;
			});
	},
});

export const { add, clear } = counterSlice.actions;

export default counterSlice.reducer;
