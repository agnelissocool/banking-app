export interface User {
	id: number;
	name: string;
	username: string;
	hash: string;
}
