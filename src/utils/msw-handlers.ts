import { beneficiariesMock } from 'api-mocks/beneficiaries-mock';
import { loginMock } from 'api-mocks/login-mock';

export const handlers = [beneficiariesMock(), loginMock()];
